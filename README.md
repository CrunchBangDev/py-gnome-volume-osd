# pyGnomeVolumeOSD

Use Python to adjust your volume and trigger the Gnome or Cinnamon on-screen-display widget for the active input or output device


## Contents

 - [Setup](#setup)
 - [Usage](#usage)
 - [Why](#why)


## Setup

This script is designed to work on Linux systems with any of the following desktop environments:
 - [Regolith](https://regolith-linux.org/)
 - [Gnome Flashback](https://wiki.gnome.org/Projects/GnomeFlashback)
 - [Gnome3](https://www.gnome.org/)
 - [Cinnamon](https://projects.linuxmint.com/cinnamon/)

...and any of the following sound daemons:
 - [PipeWire](https://pipewire.org/)
 - [Pulseaudio](https://gitlab.freedesktop.org/pulseaudio/pulseaudio)


### System Packages

Communication with the desktop environment is achieved via D-Bus. If you use Fedora or a Debian-based distribution, this means you need to install the **`python3-dbus`** system package. For Arch-based distributions, the needed package is **`dbus-python`**.


### Project Setup

```sh
git clone https://gitlab.com/CrunchBangDev/pyGnomeVolumeOSD.git
cd pyGnomeVolumeOSD
python -m pip install -r requirements.txt
python volume-osd.py
```

Note that you may need to use `python3` instead of `python` depending on your distribution. To stop needing to do this on Debian-based systems, install the system package `python-is-python3`.


### Compilation

You can compile the script to make it slightly more efficient like so:

```sh
python -O -m py_compile volume-osd.py
cp ./__pycache__/volume-osd*.pyc volume-osd
chmod +x ./volume-osd
```

I borrowed that idea from [James T's related project](https://github.com/jamerst/cinnamon-volume-step-osd).


## Usage

```sh
python volume-osd.py [--input/--i] [+/-change] [--toggle/--t] [--force/--f]
```

You can set the `input` flag to show or modify input volume instead of output volume:

```sh
python volume-osd.py --input
python volume-osd.py --i
```


You can change the volume by some amount by giving a positive or negative number:

```sh
python volume-osd.py 5
python volume-osd.py --input -100
```


You can toggle the mute status with the `toggle` flag:

```sh
python volume-osd.py --toggle
```


You can force out-of-bounds changes beyond 0 or 100 levels with the `force` flag:

```sh
python volume-osd.py 200 --force
```


### Usage With i3

Here are some examples for using this in an i3 config:

```i3
### Volume Control ###
set $volume-osd ~/Code/volume-osd/volume-osd

# Output Volume Control
bindsym XF86AudioRaiseVolume exec --no-startup-id $volume-osd +1
bindsym XF86AudioLowerVolume exec --no-startup-id $volume-osd -1
bindsym XF86AudioMute exec --no-startup-id $volume-osd --toggle

# Input Volume Control
bindsym Ctrl+XF86AudioRaiseVolume exec --no-startup-id $volume-osd --i +1
bindsym Ctrl+XF86AudioLowerVolume exec --no-startup-id $volume-osd --i -1
bindsym Ctrl+XF86AudioMute exec --no-startup-id $volume-osd --input --toggle
```


### Usage With gsettings/gedit

Setting up key bindings with gsettings or gedit is a good way to make yourself miserable. There isn't a good API for adding key bindings (there is a terrible one), and doing it through the GUI is not an appealing prospect for any mortal.

Various approaches to coping with the madness of this interface are discussed here:
https://askubuntu.com/questions/597395/how-to-set-custom-keyboard-shortcuts-from-terminal

Maybe consider switching from Gnome to [Regolith](https://regolith-linux.org/) so you can harness the power of i3.


## Why?

I hit hy head on a lot of things when I was a child. Well, there are other reasons, too...

I wanted fine volume control but I also like pretty and informative UI elements. Gnome did not seem to provide those things at the same time, so this happened.

Inspiration was drawn from here, where a better explanation of the problem Gnome creates is also provided:
https://github.com/garrett92895/gnome-volume-step-osd

Cinnamon users can find a fork of that script here:
https://github.com/jamerst/cinnamon-volume-step-osd

It turns out that Gnome has finally made the volume step configurable as of version 3.36 by using `gsettings`:

```sh
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-step 1
```

As far as I know, it still won't show the current percentage, though.
