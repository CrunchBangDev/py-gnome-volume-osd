import argparse
import dbus
import logging
import os
import sys

from pulsectl import Pulse, PulseVolumeInfo
from dbus.exceptions import DBusException


app = "pyGnomeVolumeOSD"
log = logging.getLogger(app)
volume_levels = {
    "high":   0.67,
    "medium": 0.33,
    "low":    0.01,
    "muted":  0,
}

parser = argparse.ArgumentParser(
    description="Control the volume with all the granularity you want"
)
parser.add_argument(
    'change',
    type=float,
    default=0.0,
    help=(
        "Change the volume by some amount"
    ),
    nargs="?"
)
parser.add_argument(
    '--input',
    help="Select input volume instead of output",
    action="store_true"
)
parser.add_argument(
    '--toggle',
    help="Toggle device mute state",
    action="store_true"
)
parser.add_argument(
    '--force',
    help="Force out-of-range volume changes",
    action="store_true"
)

args = parser.parse_args()

# Get the pulseaudio information for the active input or output
with Pulse(app) as pulse:
    if args.input:
        device_list = pulse.source_list()
        icon_type = 'microphone-sensitivity'
    else:
        device_list = pulse.sink_list()
        icon_type = 'audio-volume'

    try:
        # Get the first device that isn't idle
        device = next(filter(lambda x: x.state != "idle", device_list))
    except (StopIteration):
        log.error("Failed getting active audio device information")
        exit(1)
    device_description = device.port_active.description

    # Get initial volume level
    volume = device.volume.value_flat

    # Apply any requested change to volume level
    change = args.change/100
    if not args.force:
        new_change = min(change, 1-volume) if change > 0 else max(change, -volume)
        if change != new_change:
            log.warning(f"Bounded change to stay in range: {change}=>{new_change}")
            change = new_change
    pulse.volume_change_all_chans(device, change)
    volume += change
    if volume != device.volume.value_flat:
        log.warning("Audio daemon limited a requested change")
        # Update volume level
        volume = device.volume.value_flat

    # Update mute status
    if args.toggle:
        pulse.mute(device, mute=(not device.mute))

    # Get mute status
    muted = bool(device.mute)

# Determine coarse volume level
for label, value in volume_levels.items():
    level = "muted"
    if volume >= value:
        level = label
        break

# Construct the icon name to represent the audio device and level
icon = '-'.join((icon_type, level, "symbolic"))

# Truncate audio device description if it's crazy long
if len(device_description) > 25:
    device_description = device_description[:22].strip() + '...'

# Add volume info to the description, making this BETTER than the default Gnome behavior!
device_description += ' (mute)' if muted else f' ({round(volume*100)}%)'

# Build the D-Bus payload
payload = {
    "icon": icon,
    "level": volume,
    "label": device_description,
}

#Connect to the desktop shell via its D-Bus interface
shell = os.environ.get('DESKTOP_SESSION').lower()
if shell in ["gnome", "gnome-flashback", "regolith"]:
    shell_path = ["org", "gnome", "Shell"]
elif shell == "cinnamon":
    shell_path = ["org", "Cinnamon"]
else:
    log.warning(f"Detected unsupported shell `{shell}` but trying anyway")
    shell_path = ["org", shell]
shell_dbus_name = '.'.join(shell_path)
shell_object_path = '/'.join(('', *shell_path))

# Start a D-Bus session
session_bus = dbus.SessionBus()

# A D-Bus proxy object is required to broker communications with the remote object
# https://dbus.freedesktop.org/doc/dbus-python/tutorial.html#proxy-object
try:
    proxy = session_bus.get_object(shell_dbus_name, shell_object_path)
    interface = dbus.Interface(proxy, shell_dbus_name)
    # Send the payload to D-Bus
    interface.ShowOSD(payload)
except DBusException as e:
    log.error(f"Failed connecting to D-Bus: {e}")
